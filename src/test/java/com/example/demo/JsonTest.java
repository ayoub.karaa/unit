package com.example.demo;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.assertj.core.api.Assertions;
import org.junit.Test;

public class JsonTest {

    @Test
    public void testJson(){
        String response = "[\n" +
                "  {\n" +
                "    \"id\": 1 ,\n" +
                "    \"name\": \"pencil\",\n" +
                "    \"quantity\": 5\n" +
                "\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 2 ,\n" +
                "    \"name\": \"pen\",\n" +
                "    \"quantity\": 15\n" +
                "\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 3 ,\n" +
                "    \"name\": \"eraser\",\n" +
                "    \"quantity\": 10\n" +
                "\n" +
                "  }\n" +
                "]";
        DocumentContext doc = JsonPath.parse(response);
        int length = doc.read("$.length()");
        Assertions.assertThat(length).isEqualTo(3);
    }
}
