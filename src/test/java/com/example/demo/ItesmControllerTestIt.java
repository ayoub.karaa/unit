package com.example.demo;

import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;
@RunWith(SpringRunner.class)
@SpringBootTest()
public class ItesmControllerTestIt {

    private TestRestTemplate restTemplate;

    @Test
    public void contextLoad(){
       String respons = this.restTemplate.getForObject("/item/all" ,String.class);
        try {
            JSONAssert.assertEquals("[{id:1},{id:2},{id:3}]",respons,false);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }




}
