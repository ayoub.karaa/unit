package com.example.demo;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class ItemBusesTest {

    @InjectMocks
    private ItemService itemService;

    @MockBean
    private ItemRepository itemRepository;


    @Test
    public void allItemsTest() {
        when(itemRepository.findAll()).thenReturn(
                Arrays.asList(new Item(1,"ayoub1",111,1),
                        new Item(2,"ayoub2",222,1),
                        new Item(3,"ayoub3",333,1))

        );
        List<Item> items = itemService.returnAllItems() ;
        Assert.assertEquals(111,items.get(0).getValue());
        Assert.assertEquals(222,items.get(1).getValue());

    }

}
