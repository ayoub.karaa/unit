package com.example.demo;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import sun.plugin.javascript.navig.Array;

import java.util.ArrayList;
import java.util.Arrays;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ItemController.class)
public class ItemControllerTest {


    @Autowired
    private MockMvc mockMvc;
    @MockBean
    ItemService itemService;

   /* @Test
    public void hello_basictest() throws Exception{
        // call /hello
        RequestBuilder request = MockMvcRequestBuilders.get("/hello").accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(request).andReturn();
        assertEquals("hello ayoub",result.getResponse().getContentAsString());
    }*/

    @Test
    public void item_basictest2() throws Exception{
        // call /hello
        RequestBuilder request = MockMvcRequestBuilders
                .get("/item")
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(request)
                .andExpect(status().is(200))
               // .andExpect(content().string("{\"id\":1,\"name\":\"ayoub\",\"price\":10,\"quantity\":100}"))
                .andExpect(content().json("{\"id\":  1,\"name  \":\"ayoub\",\"price\":10,\"quantity\":100}"))
                .andReturn();
       // assertEquals("hello ayoub",result.getResponse().getContentAsString());
    }

    @Test
    public void allItemsTest() throws Exception{
       when(itemService.returnAllItems()).thenReturn(
               Arrays.asList(new Item(1,"ayoub1",111,1),
                             new Item(2,"ayoub2",222,1),
                             new Item(3,"ayoub3",333,1))

               );
       RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/item/all").accept(MediaType.APPLICATION_JSON);
       MvcResult result = mockMvc.perform(requestBuilder)
               .andExpect(status().isOk())
               .andExpect(content().json("[{\"id\":1,\"name\":\"ayoub1\",\"price\":111,\"quantity\":1,\"value\":0},{\"id\":2,\"name\":\"ayoub2\",\"price\":222,\"quantity\":1,\"value\":0},{\"id\":3,\"name\":\"ayoub3\",\"price\":333,\"quantity\":1,\"value\":0}]"))
               .andReturn();



    }

}
