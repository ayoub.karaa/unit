package com.example.demo;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class HamcrestTest {

    @Test
    public void test(){
        List<Integer> numbers = Arrays.asList(12,15,45);
        assertThat(numbers, hasSize(3));
        assertThat(numbers, hasItem(12));
        assertThat(numbers, everyItem(greaterThan(10)));
        //assertThat(numbers, everyItem(greaterThan(100)));
        assertThat("", isEmptyString());
        assertThat("ABCDE", containsString("BCD"));
        Assertions.assertThat(numbers).hasSize(3).contains(12).allMatch(x-> x > 10);
    }
}
