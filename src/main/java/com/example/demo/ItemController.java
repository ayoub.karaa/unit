package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ItemController {
    @Autowired
    ItemService itemService;


    @GetMapping("/item")
    public  Item  helloItem(){
        return new Item(1,"ayoub",10,100);
    }

    @GetMapping("/item/all")
    public List<Item> getAll(){

        return itemService.returnAllItems();
    }


}
