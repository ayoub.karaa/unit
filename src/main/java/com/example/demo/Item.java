package com.example.demo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.lang.Nullable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
@Entity
@Data
@Builder
@AllArgsConstructor
public class Item {
    @Id
    private int id ;
    private String name ;
    private int  price;
    private int quantity ;
    @Nullable
    @Transient
    private int value ;
    protected Item(){
    }
    public Item(int id, String name, int price, int quantity) {
        this.id=id;
        this.name=name;
        this.price=price;
        this.quantity=quantity;


    }
}
