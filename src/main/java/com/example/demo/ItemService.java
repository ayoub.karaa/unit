package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ItemService {
    @Autowired
    private ItemRepository itemRepository;

    public Item hardCodedItem(){
        return new Item(7,"mahboub",698,97);
    }

    public List<Item> returnAllItems(){
        List<Item>items = itemRepository.findAll();
        for (Item i:items)
        {
            i.setValue(i.getPrice()*i.getQuantity());

        }
       return items;

    }
}
