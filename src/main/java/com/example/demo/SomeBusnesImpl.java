package com.example.demo;

import java.util.Arrays;

public class SomeBusnesImpl {

    public int calculateSumdata (int[] data){
        return  Arrays.stream(data).reduce(Integer::sum).orElse(0);

    }
    public int calculateSum (int[] data){
        int sum = 0;
        for (int i : data){
            sum = sum+i;

        }
        return sum;
    }

}
